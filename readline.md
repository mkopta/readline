Moving
======
* `Ctrl-a` - Move to the start of the current line.
* `Ctrl-e` - Move to the end of the line.
* `Ctrl-f` - Move forward a character.
* `Ctrl-b` - Move back a character.
* `Alt-f` - Move forward to the end of the next word.
* `Alt-b` - Move back to the start of the current or previous word.
* `Ctrl-l` - Clear the screen.

History
=======
* `Ctrl-p` - Fetch the previous command from the history list.
* `Ctrl-n` - Fetch the next command from the history list.
* `Ctrl-r` - Search backward through history.
* `Ctrl-s` - Search forward through history.
* `Alt-.` - Insert the last argument to the previous command.

Changing Text
=============
* `Ctrl-v` - Add the next character you type verbatim.
* `Ctrl-t` - Transpose characters.
* `Alt-t` - Transpose words.


Killing and Yanking (Cutting and Pasting)
=========================================
* `Ctrl-k` - Kill (cut) forwards to the end of the line.
* `Ctrl-u` -  Kill (cut) backwards to the start of the line.
* `Ctrl-w` -  Kill (cut) backwards to the start of the current word.

Completing
==========
* `Tab` - Autocomplete.
* `Alt-?` - List possible completions.
* `Alt-*` - Insert possible completions.

Miscellaneous
=============
* `Ctrl-]` - Search forwards for a single character in the current line and jump to that location.
* `Ctrl-Alt-]` - Search backwards for a single character in the current line and jump to that location.
* `Alt-#` - Comment the current line and start a new one.
